﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Javascript.aspx.cs" Inherits="assignment2.Javascript" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Idea" runat="server">
    <div class="jumbotron">
<h3><u><b>JAVASCRIPT</b></u></h3>
    <p>JavaScript is one of the 3 languages all web developers must learn:<br />
           1. HTML to define the content of web pages<br />
           2. CSS to specify the layout of web pages<br />
           3. JavaScript to program the behavior of web pages<br />
        Web pages are not the only place where JavaScript is used. Many desktop and server programs use JavaScript. 
        Node.js is the best known. Some databases, like MongoDB and CouchDB, also use JavaScript as their programming language.</p>
</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Code1" runat="server">
    <div class="jumbotron">
      <h3><u> My Code:</u></h3>
         var userName=prompt("USERNAME");<br />
        console.log(userName);<br />

        var passWord=prompt("PASSWORD");<br />
        console.log(passWord); <br />


        var monkey = "monkey";<br />
        var banana = "banana";<br />

        if(userName === monkey && passWord === banana){<br />
         alert("Welcome Back!");<br />
         console.log("Login Successful");<br />
        }else {<br />
	        alert("invalid username/password");<br />
	        console.log("login fail");<br />
        }
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Code2" runat="server">
    <div class="jumbotron">
     <h3><u>Code I Found:</u></h3>
         &lt!DOCTYPE html&gt<br />
        &lt html&gt<br />
        &lt body&gt<br />

        &lt h2 &gt My First JavaScript&lt/h2&gt<br />

        &lt button type="button"<br />
        onclick="document.getElementById('demo').innerHTML = Date()"&gt<br />
        Click me to display Date and Time. &lt button&gt<br />

        &lt p id="demo"&gt &lt /p &gt<br />

        &lt body&gt<br />
        &lt html&gt<br />
</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Links" runat="server">
    <div class="jumbotron">
    <h3><u>Some Useful Links:</u></h3>
    <ul class="html_links">
   <li><a href="https://www.codecademy.com/learn/learn-javascript">Codecademy</a></li>
   <li><a href="https://www.w3schools.com/">W3Schools</a></li>
   <li><a href="https://javascript.info/">JAVASCRIPT.info</a></li>
   </ul>
 </div>
</asp:Content>

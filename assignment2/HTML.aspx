﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HTML.aspx.cs" Inherits="assignment2.HTML" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Idea" runat="server">
    <div class="jumbotron">
     <h3><u><b>HTML & CSS</b></u></h3>
      <p>The aside element represents a section of a page that consists of content that is tangentially related to the content around the aside element, and which could be considered separate from that content. Such sections are often represented as sidebars in printed typography.
    The element can be used for typographical effects like pull quotes or sidebars, for advertising, for groups of nav elements, and for other content that is considered separate from the main content of the page.
    </p>
   </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Code1" runat="server">
     <div class="jumbotron">
        <h3><u> My Code:</u></h3>
         &lt !DOCTYPE html &gt<br/>
         &lt html &gt<br/>
         &lt body &gt<br/>
        &lt"aside id="sidebar-1"&gt<br/>
		&lt h3&gt FOOD TRAVELS &lt/h3&gt <br />
		&lt nav id="sidebar-one-menu"&gt <br />
	    &lt ul class="menu-one"&gt <br />
		&lt li&gt &lt a href="#"&gt Traditional cod cakes in Gaspe &lt/a&gt &lt/li&gt <br />
        &lt li&gt &lt a href="#"&gt Fresh baguettes on the streets of Paris &lt/a&gt &lt/li&gt <br />
		&lt li&gt &lt a&gt &lt href="#"&gt Lobster and shellfish in Atlantic Canada &lt a&gt &lt/li&gt <br />
		&lt li&gt &lt a href="#" &gt Clamming for family fun &lt/a&gt &lt/li&gt <br />
		&lt/ul&gt<br/>
		&lt/nav&gt<br/>
		&lt/aside&gt<br />
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Code2" runat="server">
     <div class="jumbotron">
     <h3><u>Code I Found:</u></h3>
             &lt !DOCTYPE html&gt<br />
            &lt html&gt<br />
            &lt body&gt<br />

            &lt p &gt My family and I visited The Epcot center this summer.&lt /p &gt<br />

            &lt aside&gt<br />
              &lt h4&gt Epcot Center &lt/h4&gt<br />
              &lt p&gt The Epcot Center is a theme park in Disney World, Florida.&lt/p&gt<br />
            &lt /aside&gt<br />

            &lt /body &gt<br />
            &lt /html &gt<br />
    </div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Links" runat="server">
     <div class="jumbotron">
    <h3><u>Some Useful Links:</u></h3>
    <ul class="html_links">
   <li><a href="https://learn.shayhowe.com/">Learn to Code HTML & CSS</a></li>
   <li><a href="https://www.w3schools.com/">W3Schools</a></li>
   <li><a href="https://html.com/">HTML.com</a></li>
   </ul>
   </div>
</asp:Content>

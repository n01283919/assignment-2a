﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SQL.aspx.cs" Inherits="assignment2.SQL" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Idea" runat="server">
     <div class="jumbotron">
         <h3><u><b>DATABASE</b></u></h3>
         <p>Most databases are accessed using the Structured Query Language (SQL). 
        SQL can either be written into server-side code (like PHP or ASP) or run through a GUI like SQL Developer.</p>
     </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Code1" runat="server">
     <div class="jumbotron">
         <h3><u> My Code:</u></h3>
        <p>SELECT invoice_number, invoice_date, invoice_total<br /> 
        FROM invoices<br />
        WHERE invoice_total > credit_total<br />
        OR invoice_total - payment_total - credit_total > 0<br />
        AND terms_id NOT IN (2, 5)<br /></p>
     </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Code2" runat="server">
     <div class="jumbotron">
         <h3><u>Code I Found:</u></h3>
         <p>INSERT INTO Customers (CustomerName, ContactName, Address, City, PostalCode, Country)<br />
             VALUES ('Cardinal','Tom B. Erichsen','Skagen 21','Stavanger','4006','Norway');<br /></p>
     </div> 
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Links" runat="server">
    <div class="jumbotron">
    <h3><u>Some Useful Links:</u></h3>
    <ul class="html_links">
   <li><a href="https://simonborer.github.io/db-course/">SIMON'S</a></li>
   <li><a href="https://www.w3schools.com/">W3Schools</a></li>
   <li><a href="https://www.udemy.com/learn-database-design-with-mysql/">Udemy</a></li>
   </ul>
   </div>
</asp:Content>
